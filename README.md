# Employees Directory

Simple implementation of employee directory with departments Listing


## Installation

```bash
git clone git@gitlab.com:employee-directory/frontend-react.git
cd frontend-react
yarn
```

## Get started

```bash
yarn start
```
